"Global
:syntax enable
:set ruler
:set scrolloff=2
:set shell=/bin/bash
:set shiftround
:set showcmd
:set whichwrap=b,s,h,l,<,>,[,]
:set backspace=indent,eol,start
":set redrawtime=500
:set undolevels=100
:set lazyredraw

"Local to buffer
:set autoindent
:set shiftwidth=2
:set tabstop=2
:set expandtab

"Local to window
:set breakindent
:set breakindentopt=shift:1
:set number

:filetype plugin indent on

:nmap <F5> :set rnu!<CR>
:imap <F5> <Esc><F5>i
:vmap <F5> <Esc><F5>gv

autocmd BufEnter * :syntax sync minlines=1

:let g:tex_flavor='latex'
