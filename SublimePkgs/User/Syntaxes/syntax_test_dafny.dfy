// SYNTAX TEST "Packages/User/Syntaxes/dafny.sublime-syntax"

method MultipleReturns(x: int, y: int) returns (more: int, less: int)
// <- meta.function.dafny storage.type.dafny
//     ^^^^^^^^^^^^^^^ meta.function.dafny entity.name.function.dafny
//                    ^ punctuation.section.group.begin.dafny
//                     ^^^^^^^^^^^^^^ meta.function.parameters.dafny meta.group.dafny
//                     ^ variable.parameter.dafny
//                      ^ keyword.operator.type.dafny
//                        ^^^ storage.type.dafny
//                           ^ punctuation.separator.dafny
//                                   ^ punctuation.section.group.end.dafny
//                                     ^^^^^^^ keyword.other.dafny
//                                              ^^^^^^^^^^^^^^^^^^^^ meta.function.return-type.dafny meta.group.dafny
  ensures less < x && x < more
//             ^ keyword.operator.logical.dafny
//                 ^^ keyword.operator.logical.dafny
{
// <- meta.function meta.block punctuation.section.block.begin.dafny
  more := x + y;
//^^^^ variable.other.dafny
//     ^^ keyword.operator.assignment.dafny
//        ^ variable.other.dafny
//          ^ keyword.operator.arithmetic.dafny
//             ^ punctuation.terminator.dafny
  less := x - y;
//     ^^ keyword.operator.assignment.dafny
//          ^ keyword.operator.arithmetic.dafny
//             ^ punctuation.terminator.dafny
  
  // comments: are not strictly necessary.
//^^ punctuation.definition.comment.dafny
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.dafny
  
  /* unless you want to keep your sanity. */
//^^ punctuation.definition.comment.dafny
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.block.dafny
//                                        ^^ punctuation.definition.comment.dafny
}
// <- meta.function meta.block punctuation.section.block.end.dafny
  // comments: are not strictly necessary.
//^^ punctuation.definition.comment.dafny
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.dafny
  
  /* unless you want to keep your sanity. */
//^^ punctuation.definition.comment.dafny
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.block.dafny
//                                        ^^ punctuation.definition.comment.dafny

method Abs(x: int) returns (y: int)
  ensures 0 <= y
//^^^^^^^ meta.function.dafny keyword.other.dafny
{
  if x < 0
//^^ keyword.control.dafny
    { return -x; }
//  ^ punctuation.section.block.begin.dafny
//  ^^^^^^^^^^^^^^ meta.block.dafny meta.block.dafny
//               ^ punctuation.section.block.end.dafny
  else
//^^^^ keyword.control.dafny
    { return x; }
//    ^^^^^^ keyword.control.dafny
}

method Testing(a: array<int>)
  requires a != null
//              ^^^^ constant.language.dafny
  requires n == Factorial(a)
//              ^^^^^^^^^^^^ meta.function-call.dafny
//              ^^^^^^^^^ variable.function.dafny
  ensures a == thing.MakeDo()
//             ^^^^^^^^^^^^^^ meta.function-call.dafny
//             ^^^^^ variable.other.dafny
//                   ^^^^^^ variable.function.dafny
  ensures a.Length >= 4
//          ^^^^^^ variable.other.member.dafny
{
  assert 2 < 3;
//^^^^^^ support.function.dafny
  assert (3 < 4);
//       ^ punctuation.section.group.begin.dafny
//       ^^^^^^^ meta.group.dafny
//             ^ punctuation.section.group.end.dafny

  var b := false;
  var x, y, z: bool := 1, 2, true;
//                      ^ punctuation.separator.dafny
//                           ^^^^ constant.language.dafny
  if (a == b)
//   ^ punctuation.section.group.begin.dafny
//   ^^^^^^^^ meta.group.dafny
//          ^ punctuation.section.group.end.dafny
    { return x; }
  else if a < b
//^^^^ keyword.control.dafny
//     ^^ keyword.control.dafny
    { return y; }
//  ^ punctuation.section.block.begin.dafny
//  ^^^^^^^^^^^^^ meta.block.dafny meta.block.dafny
  else if (a > b)
//        ^ punctuation.section.group.begin.dafny
//        ^^^^^^^ meta.group.dafny
//              ^ punctuation.section.group.end.dafny
    { return z; }
  else
    { return 0; }
//  ^^^^^^^^^^^^^ meta.block.dafny meta.block.dafny
  
  assert abs(3) == 3;
//       ^^^^^^ meta.function-call.dafny
//       ^^^ variable.function.dafny
  
  x := a.Length;
//     ^ variable.other.dafny
//      ^ punctuation.accessor.dafny
//       ^^^^^^ variable.other.member.dafny

  thing.DoSomething(a, b + c, x * y);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call.dafny
//^^^^^ variable.other.dafny
//     ^ punctuation.accessor.dafny
//      ^^^^^^^^^^^ variable.function.dafny
//                 ^^^^^^^^^^^^^^^^^ meta.group.dafny
  
  if a.Length < 4 && thing.test(x)
//    ^ punctuation.accessor.dafny
//     ^^^^^^ variable.other.member.dafny
//                   ^^^^^^^^^^^^^ meta.function-call.dafny
  {
    thing.DoSomethingElse(x, y);
//  ^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call.dafny
  }
  
  if (a.Length < 4 && thing.test(x))
//     ^ punctuation.accessor.dafny
//      ^^^^^^ variable.other.member.dafny
//                    ^^^^^^^^^^^^^ meta.function-call.dafny
  {
    thing.DoSomethingElse(x, y);
//  ^^^^^^^^^^^^^^^^^^^^^^^^^^^ meta.function-call.dafny
  }
}

function max(a: int, b: int): int
//                          ^ keyword.operator.type.dafny
//                            ^^^ storage.type.dafny
{
  // Fill in an expression here.
}

method m(n: nat)
{
  var i := 0;
  while i < n
//^^^^^ keyword.control.dafny
  {
//^ meta.block.dafny meta.block.dafny punctuation.section.block.begin.dafny
    i := i + 1;
  }
//^ punctuation.section.block.end.dafny

  while (i < n)
//^^^^^ keyword.control.dafny
//      ^^^^^^^ meta.group.dafny
  {
//^ meta.block.dafny meta.block.dafny punctuation.section.block.begin.dafny
    i := i + 1;
  }
//^ punctuation.section.block.end.dafny
}

