import gdb

__STOP_REG__ = False
__STOP_BT__= False
__BREAK_REG__ = False
__BREAK_BT__ = False

def stopHandler (event):
  global __BREAK_BT__, __BREAK_REG__, __STOP_BT__, __STOP_REG__
  
  if isinstance(event, gdb.BreakpointEvent):
    gdb.write("\n   EXECUTION FLOW STOPPED - BREAKPOINT REACHED\n\n")
    if __BREAK_BT__:
      gdb.write("   Backtrace:\n")
      gdb.execute("bt")
      gdb.write("\n\n")
    if __BREAK_REG__:
      gdb.write("   Register info:\n")
      gdb.execute("i r")
      gdb.write("\n\n")
  else:
    if __STOP_BT__:
      gdb.write("   Full Backtrace:\n")
      gdb.execute("bt f")
      gdb.write("\n\n")
    if __STOP_REG__:
      gdb.write("   Register info:\n")
      gdb.execute("i r")
      gdb.write("\n\n")

gdb.events.stop.connect(stopHandler)


class SetStop (gdb.Command):
  global __STOP_BT__, __STOP_REG__
  def __init__(self):
    super (SetStop, self).__init__ ("setstop", gdb.COMMAND_USER)
  
  def printInfo(self):
    global __STOP_BT__, __STOP_REG__
    gdb.write("Register info at execution stop is ")
    if __STOP_REG__:
      gdb.write("enabled\n")
    else:
      gdb.write("disabled\n")
    
    gdb.write("Backtrace at execution stop is ")
    if __STOP_BT__:
      gdb.write("enabled\n");
    else:
      gdb.write("disabled\n");
    
    gdb.write("(Breakpoints have their own settings. Use setbreak for info.)\n")
  
  def invoke(self, arg, from_tty):
    global __STOP_BT__, __STOP_REG__
    if arg == "none":
      __STOP_REG__ = False
      __STOP_BT__ = False
      gdb.write("  Info printing at execution stop DISABLED\n")
      
    elif arg == "bt" or arg == "backtrace":
      __STOP_BT__ = not __STOP_BT__
      gdb.write("  Backtrace at execution stop ")
      if __STOP_BT__:
        gdb.write("ENABLED\n")
      else:
        gdb.write("DISABLED\n")
    
    elif arg == "reg" or arg == "registers":
      __STOP_REG__ = not __STOP_REG__
      gdb.write("  Register info at execution stop ")
      if __STOP_REG__:
        gdb.write("ENABLED\n")
      else:
        gdb.write("DISABLED\n")
    
    self.printInfo()


class SetBreak (gdb.Command):
  global __BREAK_BT__, __BREAK_REG__
  def __init__(self):
    super (SetBreak, self).__init__ ("setbreak", gdb.COMMAND_USER)
  
  def printInfo(self):
    global __BREAK_BT__, __BREAK_REG__
    gdb.write("Register info at breakpoint is ")
    if __BREAK_REG__:
      gdb.write("enabled\n")
    else:
      gdb.write("disabled\n")
    
    gdb.write("Backtrace at breakpoint is ")
    if __BREAK_BT__:
      gdb.write("enabled\n");
    else:
      gdb.write("disabled\n");
    
    gdb.write("(General execution stops have their own settings. Use setstop for info.)\n")
  
  def invoke(self, arg, from_tty):
    global __BREAK_BT__, __BREAK_REG__
    if arg == "none":
      __BREAK_BT__ = False
      __BREAK_REG__ = False
      gdb.write("  Info printing at breakpoint DISABLED\n")
      
    elif arg == "bt" or arg == "backtrace":
      __BREAK_BT__ = not __BREAK_BT__
      gdb.write("  Backtrace at breakpoint ")
      if __BREAK_BT__:
        gdb.write("ENABLED\n")
      else:
        gdb.write("DISABLED\n")
    
    elif arg == "reg" or arg == "registers":
      __BREAK_REG__ = not __BREAK_REG__
      gdb.write("  Register info at breakpoint ")
      if __BREAK_REG__:
        gdb.write("ENABLED\n")
      else:
        gdb.write("DISABLED\n")
    
    self.printInfo()

SetStop()
SetBreak()
