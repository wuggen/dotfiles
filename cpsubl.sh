#!/bin/bash

path=`convert-path $APPDATA`/Sublime\ Text\ 3/Packages

if [ "$1" != "-r" ] ; then
  echo Copying \~/SublimePkgs/User to $path
  cp -rf ~/SublimePkgs/User "$path"
else
  echo Copying $path/User to \~SublimePkgs
  cp -rf "$path/User" ~/SublimePkgs
fi

# -e s,\\\\,/,g
