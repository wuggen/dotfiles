# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# ~/.bashrc: executed by bash(1) for interactive shells.

# The copy in your home directory (~/.bashrc) is yours, please
# feel free to customise it to create a shell
# environment to your liking.  If you feel a change
# would be benifitial to all, please feel free to send
# a patch to the msys2 mailing list.

# User dependent .bashrc file

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

alias df='df -h'
alias du='du -h'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

if [ -f "${HOME}/.bash_functions" ]; then
  source "${HOME}/.bash_functions"
fi

shopt -s direxpand
shopt -s completion_strip_exe

export PATH=${PATH}":/c/Program Files/MikTeX 2.9/miktex/bin/x64:\
/c/Program Files (x86)/Dr. Memory/bin:\
/c/Program Files/Sublime Text 3:\
/c/Tor/Tor:\
/c/MongoDB/Server/3.4/bin:\
/c/ProgramData/Anaconda3:\
/c/ProgramData/Anaconda3/Scripts:\
/c/Program Files/Haskell Platform/bin:\
/c/Users/${USER}/AppData/Roaming/local/bin:\
/c/Program Files/Haskell Platform/lib/extralibs/bin:\
/c/erl9.1/bin:\
/c/Program Files/swipl/bin:\
/c/Users/${USER}/AppData/Local/Keybase:\
/c/Program Files/Java/jdk1.8.0_181/bin"

[ "$USER" = "John" ] && GD='/e/GoogleDrive'
[ "$USER" = "andrej6" ] && GD='/c/johnstuff/drive'
DESKTOP="/c/Users/${USER}/Desktop"

#APPDATA=$(cygpath -u ${APPDATA})

alias latex='latexmk -pdfxe'
alias py='python'
alias py3='python3'
alias py2='python2'
alias pdb='py -m pdb'

alias ls='ls -F --color=tty'
alias lsl='ls -lh'
alias lsa='ls -A'
alias lsal='ls -Alh'
alias lsla='lsal'

alias gcc='gcc -fdiagnostics-color'
alias g++='g++ -fdiagnostics-color'

alias cl='clear'

alias clip='clip.exe'
alias hexdump='hexdump -C'

source ${HOME}/.git-completion.bash
source ${HOME}/.git-prompt.sh
source ${HOME}/.rust.bash

GIT_PS1_SHOWDIRTYSTATE=true

export PROMPT_COMMAND='__git_ps1 "\n\[\e[36;1m\](\j,${SHLVL}) \[\e[0m\]\[\e[32m\]${MSYSTEM} \[\e[34;1m\]\u \[\e[31;1m\]\w" "\[\e[34;1m\]\n  \$\[\e[0m\] " " \[\e[35m\](branch %s) "'
