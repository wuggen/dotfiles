#!/bin/bash

texworks() (
  texworks.exe $@ &
)

drmemory_plus() {
  [[ "$1" = "ov" ]] && { shift; drmemory.exe $@; return; }

  [[ -d ./logs ]] || mkdir logs
  drmemory.exe -lib_blacklist '*ConEmuHk.dll*' -lib_blacklist_frames 4 -batch -logdir ./logs -- $@
}
alias drmemory='drmemory_plus'

explorer() {
  if [ -z "$1" ]; then
    /c/Windows/explorer.exe .
  else
    while [ ! -z "$1" ]; do
      path=$(cygpath -wa "$1")
      /c/Windows/explorer.exe $path
      shift
    done
  fi
}

trim () {
  for f in $@ ; do
    sed -r 's,\s+$,,g' $f > $TEMP/$f~
    mv $TEMP/$f~ $f
  done
}

rlrc () {
  pushd ~
  source /etc/profile
  source ~/.bashrc
  popd
}

condaset () {
  if (( $# == 0 )); then
    echo Deactivating conda env
    if [ -z $CONDA_DEFAULT_ENV ] ; then return ; fi
    export PATH="$CONDA_PATH_BACKUP"
    unset CONDA_PATH_BACKUP
    unset CONDA_DEFAULT_ENV
    unset CONDA_PREFIX
    hash -r
    return
  fi

  echo Activating env $1
  if [ -z $CONDA_DEFAULT_ENV ] ; then
    echo Saving PATH as CONDA_PATH_BACKUP
    export CONDA_PATH_BACKUP="$PATH"
  else
    echo Overwriting PATH with CONDA_PATH_BACKUP
    export PATH="$CONDA_PATH_BACKUP"
  fi

  export CONDA_DEFAULT_ENV="$1"
  export PATH="$(conda ..activate bash.exe $1):$PATH"
  export CONDA_PREFIX=${PATH%%:*}
}
