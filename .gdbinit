source ~/.gdbpy.py

set print pretty on

python
import sys
import os
sys.path.insert(0, 'c:/msys64/home/' + os.getlogin() + '/gdb_printers/python')
from libstdcxx.v6.printers import register_libstdcxx_printers
register_libstdcxx_printers(None)
end

document setbreak
  Sets automatic data display options for breakpoints.
  Passed 'n', disables automatic data display on breakpoints. Passed 't',
  GDB will display a full backtrace upon reaching a breakpoint.
  Passed 'r', GDB will display register info upon reaching a breakpoint.
  Passed 'b', GDB will display both registers and a full backtrace. Options
  set by setbreak are superceded by those set by setstop. Alias: stb
end

document setbreak
  Sets automatic data display options for program execution stops.
  Passed 'n', disables automatic data display on stopping execution flow. Passed 't',
  GDB will display a full backtrace upon stopping execution flow.
  Passed 'r', GDB will display register info upon stopping execution flow.
  Passed 'b', GDB will display both registers and a full backtrace. Options
  set by setbreak are superceded by those set by setstop. Alias: sts
end
