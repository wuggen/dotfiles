
AL_STATIC := -static
AL_DYNAM := .dll
AL_DEBUG_COMP := -debug

AL_LINKAGE = $(AL_DYNAM)
AL_DEBUG =
AL_SUFFIX = $(AL_DEBUG)$(AL_LINKAGE)

AL_BASE = -lallegro$(AL_SUFFIX)
AL_ACODEC = -lallegro_acodec$(AL_SUFFIX)
AL_AUDIO = -lallegro_audio$(AL_SUFFIX)
AL_COLOR = -lallegro_color$(AL_SUFFIX)
AL_DIALOG = -lallegro_dialog$(AL_SUFFIX)
AL_FONT = -lallegro_font$(AL_SUFFIX)
AL_IMAGE = -lallegro_image$(AL_SUFFIX)
AL_MAIN = -lallegro_main$(AL_SUFFIX)
AL_MEMFILE = -lallegro_memfile$(AL_SUFFIX)
AL_MONOLITH = -lallegro_monolith$(AL_SUFFIX)
AL_PHYSFS = -lallegro_physfs$(AL_SUFFIX)
AL_PRIMITIVES = -lallegro_primitives$(AL_SUFFIX)
AL_TTF = -lallegro_ttf$(AL_SUFFIX)
AL_VIDEO = -lallegro_video$(AL_SUFFIX)
