#!/bin/bash

if [ "$1" == "dtl" ] ; then
  sed s,'D:\\GoogleDrive\\Journey\.(Game)\.full\.1396122\.jpg','C:\\johnstuff\\drive\\Journey wallpaper\.png',g ConEmuSettings.xml > tempConEmuSettings.xml~
  sed s,'"bgImageDarker" type="hex" data="0c"','"bgImageDarker" type="hex" data="20"',g tempConEmuSettings.xml~ > tempConEmuSettings.xml
  rm tempConEmuSettings.xml~
elif [ "$1" == "ltd" ] ; then
  sed s,'C:\\johnstuff\\drive\\Journey wallpaper\.png','D:\\GoogleDrive\\Journey\.(Game)\.full\.1396122\.jpg',g ConEmuSettings.xml > tempConEmuSettings.xml~
  sed s,'"bgImageDarker" type="hex" data="20"','"bgImageDarker" type="hex" data="0c"',g tempConEmuSettings.xml~ > tempConEmuSettings.xml
  rm tempConEmuSettings.xml~
else
  echo Unrecognized command
fi
